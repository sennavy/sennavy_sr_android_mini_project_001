package com.example.androidminiproject.api;

import com.example.androidminiproject.entities.Article;
import com.example.androidminiproject.entities.response.ArticleImageResponse;
import com.example.androidminiproject.entities.response.ArticleListResponse;
import com.example.androidminiproject.entities.response.ResponseArticle;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ArticleService {
    @GET("v1/api/articles")
    Call<ArticleListResponse> findAllArticles(@Query("page") int page);

    @GET("v1/api/articles/{id}")
    Call<ResponseArticle> findArticleById(@Path("id")int id);

    @POST("v1/api/articles")
    Call<Article> addArticle(@Body Article article);

    @PUT("v1/api/articles/{id}")
    Call<Article> updateArticle(@Path("id") int id, @Body Article article);

    @DELETE("v1/api/articles/{id}")
    Call<ResponseBody> deleteArticle(@Path("id") int id);

    @Multipart
    @POST("v1/api/uploadfile/single")
    Call<ArticleImageResponse> uploadImage(@Part MultipartBody.Part image);

}
