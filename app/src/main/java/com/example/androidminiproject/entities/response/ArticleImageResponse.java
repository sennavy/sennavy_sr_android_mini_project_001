package com.example.androidminiproject.entities.response;

import com.google.gson.annotations.SerializedName;

public class ArticleImageResponse {
    @SerializedName("CODE")
    String code;
    @SerializedName("MESSAGE")
    String message;
    @SerializedName("DATA")
    String data;

    public ArticleImageResponse(String code, String message, String data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
