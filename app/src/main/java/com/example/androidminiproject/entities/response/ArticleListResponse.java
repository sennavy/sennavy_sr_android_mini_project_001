package com.example.androidminiproject.entities.response;

import com.example.androidminiproject.entities.Article;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ArticleListResponse {

    @SerializedName("DATA")
    private List<Article> articles;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("CODE")
    private String code;

    public ArticleListResponse(String code, String message, List<Article> articles) {
        this.articles = articles;
        this.message = message;
        this.code = code;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setData(List<Article> articles) {
        this.articles = articles;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
