package com.example.androidminiproject.entities.response;

import com.example.androidminiproject.entities.Article;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseArticle {
    @SerializedName("CODE")
    private String code;
    @SerializedName("MESSAGE")
    private String message;
    @SerializedName("DATA")
    private Article articles;
    @SerializedName("IMAGE")
    private String imageUrl;

    public ResponseArticle(String code, String message, Article articles, String imageUrl) {
        this.code = code;
        this.message = message;
        this.articles = articles;
        this.imageUrl = imageUrl;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Article getArticles() {
        return articles;
    }

    public void setArticles(Article articles) {
        this.articles = articles;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
