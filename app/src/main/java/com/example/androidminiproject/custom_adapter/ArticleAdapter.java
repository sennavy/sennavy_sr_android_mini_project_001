package com.example.androidminiproject.custom_adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.androidminiproject.R;
import com.example.androidminiproject.entities.Article;
import com.example.androidminiproject.ui.main.mvp.LoadDataPresenter;
import com.example.androidminiproject.ui.manipulation.ManipulateMainActivity;

import java.util.ArrayList;
public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.MyViewHolder>{
    private ArrayList<Article> articles;
    Context context;
    private final LoadDataPresenter presenter;


    public ArticleAdapter(ArrayList<Article> articles,Context context,LoadDataPresenter presenter) {
        this.articles = articles;
        this.context = context;
        this.presenter = presenter;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.article_item_layout,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.textTitle.setText(articles.get(position).getTitle());
        holder.textCreatedDate.setText(articles.get(position).getCreatedDate());
        holder.textDescription.setText(articles.get(position).getDescription());
        Glide.with(context)
                .load(articles.get(position).getImage())
                .centerCrop()
                .placeholder(R.drawable.default_list_image)
                .into(holder.imageView);
        holder.textOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(context,holder.textOption);
                popupMenu.inflate(R.menu.option_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.delete:
                                Log.e("TAG","Selected for delete: " + articles.get(position).getId());
                                confirmDelete(articles.get(position).getId());

                                break;
                            case R.id.edit:
                                Log.e("TAG",articles.get(position).getId() +"");
                                Intent intent = new Intent(context, ManipulateMainActivity.class);
                                intent.putExtra("articleId",articles.get(position).getId());
                                intent.putExtra("layout", R.string.edit_ui);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                context.getApplicationContext().startActivity(intent);
                                break;
                        }
                        return false;
                    }
                });
                popupMenu.show();
            }
        });
    }

    private void confirmDelete(final int id){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false)
                .setTitle("Delete Confirmation")
                .setMessage("Are you sure?");
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                presenter.onDeleteArticle(id);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView textTitle;
        TextView textCreatedDate;
        TextView textDescription;
        ImageView imageView;
        TextView textOption;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textTitle = itemView.findViewById(R.id.text_title);
            textCreatedDate = itemView.findViewById(R.id.created_date);
            textDescription = itemView.findViewById(R.id.text_description);
            imageView = itemView.findViewById(R.id.article_image);
            textOption = itemView.findViewById(R.id.text_option);
        }
    }
}
