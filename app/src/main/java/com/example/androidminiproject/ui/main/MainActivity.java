package com.example.androidminiproject.ui.main;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.example.androidminiproject.R;
import com.example.androidminiproject.entities.Article;
import com.example.androidminiproject.custom_adapter.ArticleAdapter;
import com.example.androidminiproject.ui.main.mvp.LoadDataMVP;
import com.example.androidminiproject.ui.main.mvp.LoadDataPresenter;
import com.example.androidminiproject.ui.manipulation.ManipulateMainActivity;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements LoadDataMVP.ArticleView {

    private ArrayList<Article> articleList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ArticleAdapter articleAdapter;
    private ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LoadDataPresenter presenter;
    LinearLayoutManager layoutManager;
    FloatingActionButton btnAddArticle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        onViewInitialize();
        //loading data
        onSwipeRefreshData();
        articleList = new ArrayList<>();
        presenter = new LoadDataPresenter(this);
        presenter.requestDataFromAPI();
        articleAdapter = new ArticleAdapter(articleList,this, presenter);
        recyclerView.setAdapter(articleAdapter);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //adding data
        btnAddArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ManipulateMainActivity.class);
                intent.putExtra("layout",R.string.add_ui);
                startActivity(intent);
            }
        });
    }

//    @Override
//    protected void onRestart() {
//        super.onRestart();
//        presenter.requestDataFromAPI();
//    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.requestDataFromAPI();
    }

    private void onViewInitialize(){
        recyclerView = findViewById(R.id.article_recycler_view);
        btnAddArticle = findViewById(R.id.btn_add_article);
        progressBar = findViewById(R.id.pgb_load_data);
        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
    }

    private void onSwipeRefreshData(){
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                presenter.requestDataFromAPI();
            }
        });
    }

    @Override
    public void onShowLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onHideLoading() {
        progressBar.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onSetDataToRecyclerView(List<Article> articles) {
        this.articleList.clear();
        this.articleList.addAll(articles);
        articleAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFail(Throwable t) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true)
                .setTitle("Error")
                .setMessage(t.getMessage());
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
