package com.example.androidminiproject.ui.main.mvp;

import com.example.androidminiproject.entities.Article;

import java.util.List;

public class LoadDataPresenter implements LoadDataMVP.Presenter, LoadDataMVP.Interactor.OnGetArticleInteractor{
    private LoadDataMVP.ArticleView articleView;
    private LoadDataMVP.Interactor interactor;

    public LoadDataPresenter(LoadDataMVP.ArticleView articleView) {
        this.articleView = articleView;
        interactor = new LoadDataInteractor();
    }

    @Override
    public void onGetSuccess(List<Article> articles) {
        articleView.onSetDataToRecyclerView(articles);
        articleView.onHideLoading();
    }

    @Override
    public void onGetFail(Throwable t) {
        articleView.onFail(t);
    }

    @Override
    public void requestDataFromAPI() {
        if(articleView != null){
            articleView.onShowLoading();
            interactor.findAllArticles(1,this);
        }
    }

    @Override
    public void onDeleteArticle(int id) {
        interactor.deleteArticle(id, new LoadDataMVP.Interactor.OnDeleteInteractor() {
            @Override
            public void onDeleteSuccess() {
                interactor.findAllArticles(1, new LoadDataMVP.Interactor.OnGetArticleInteractor() {
                    @Override
                    public void onGetSuccess(List<Article> articles) {
                        articleView.onSetDataToRecyclerView(articles);
                        articleView.onHideLoading();
                    }

                    @Override
                    public void onGetFail(Throwable t) {
                        articleView.onFail(t);
                    }
                });
            }
            @Override
            public void onDeleteFail(Throwable t) {
                articleView.onFail(t);
            }
        });
    }
}
