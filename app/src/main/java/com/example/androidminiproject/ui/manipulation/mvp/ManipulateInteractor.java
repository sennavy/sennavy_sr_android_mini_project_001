package com.example.androidminiproject.ui.manipulation.mvp;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.example.androidminiproject.api.ArticleService;
import com.example.androidminiproject.api.ServiceConverter;
import com.example.androidminiproject.entities.Article;
import com.example.androidminiproject.entities.response.ArticleImageResponse;
import com.example.androidminiproject.entities.response.ResponseArticle;
import com.example.androidminiproject.utils.FileUtils;

import java.io.File;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ManipulateInteractor implements ManipulateMVP.Interactor {
    ArticleService articleService = ServiceConverter.retrofit().create(ArticleService.class);

    @Override
    public void getArticleById(int id, final OnGetArticleInteractor onGetArticleInteractor) {
        Call<ResponseArticle> call = articleService.findArticleById(id);
        call.enqueue(new Callback<ResponseArticle>() {
            @Override
            public void onResponse(Call<ResponseArticle> call, Response<ResponseArticle> response) {
                onGetArticleInteractor.onGetSuccess(response.body());
            }
            @Override
            public void onFailure(Call<ResponseArticle> call, Throwable t) {
                onGetArticleInteractor.onGetFail(t);
            }
        });
    }

    @Override
    public void updateArticle(int id, Article article, final OnUpdateInteractor onUpdateInteractor) {
        Call<Article> call = articleService.updateArticle(id,article);
        call.enqueue(new Callback<Article>() {
            @Override
            public void onResponse(Call<Article> call, Response<Article> response) {
                onUpdateInteractor.onUpdateSuccess();
                Log.e("TAG","Update success");
            }
            @Override
            public void onFailure(Call<Article> call, Throwable t) {
                onUpdateInteractor.onUpdateFail(t);
                Log.e("TAG","Update failed");
            }
        });
    }

    @Override
    public void addArticle(Article article, final onAddInteractor onAddInteractor) {
        Call<Article> call = articleService.addArticle(article);
        call.enqueue(new Callback<Article>() {
            @Override
            public void onResponse(Call<Article> call, Response<Article> response) {
                onAddInteractor.onAddSuccess();
                Log.e("TAG","Add success");
            }
            @Override
            public void onFailure(Call<Article> call, Throwable t) {
                onAddInteractor.onAddFail(t);
                Log.e("TAG","Add failed");
            }
        });
    }

    @Override
    public void uploadImage(Uri image, Context context, final OnUploadImageInteractor onUploadImageInteractor) {
        File file = new File(FileUtils.getPath(image,context));
        RequestBody requestBody = RequestBody.create(MediaType.parse("form-data"),file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("FILE",file.getName(),requestBody);

        Call<ArticleImageResponse> call = articleService.uploadImage(part);
        call.enqueue(new Callback<ArticleImageResponse>() {
            @Override
            public void onResponse(Call<ArticleImageResponse> call, Response<ArticleImageResponse> response) {
                onUploadImageInteractor.onUploadImageSuccess(response.body().getData());
            }

            @Override
            public void onFailure(Call<ArticleImageResponse> call, Throwable t) {
                onUploadImageInteractor.onUploadImageFail(t);
            }
        });
    }
}
