package com.example.androidminiproject.ui.manipulation.mvp;

import android.content.Context;
import android.net.Uri;

import com.example.androidminiproject.entities.Article;
import com.example.androidminiproject.entities.response.ResponseArticle;

public interface ManipulateMVP {
    interface View{
        void onShowLoading();
        void onHideLoading();
        void onGetSuccess(Article article);
        void onFailure(Throwable t);
        void onUploadImageSuccess(String imagePath);
        void onFinishView();
    }
    interface Presenter{
        void findArticleById(int id);
        void addArticle(Article article);
        void updateArticle(int id, Article article);
        void uploadImage(Uri image, Context context);
    }

    interface Interactor{
        interface OnGetArticleInteractor{
            void onGetSuccess(ResponseArticle responseArticle);
            void onGetFail(Throwable t);
        }

        interface onAddInteractor{
            void onAddSuccess();
            void onAddFail(Throwable t);
        }

        interface OnUpdateInteractor{
            void onUpdateSuccess();
            void onUpdateFail(Throwable t);
        }

        interface OnUploadImageInteractor{
            void onUploadImageSuccess(String imagePath);
            void onUploadImageFail(Throwable t);
        }
        void getArticleById(int id,OnGetArticleInteractor onGetArticleInteractor);
        void updateArticle(int id,Article article, OnUpdateInteractor onUpdateInteractor );
        void addArticle(Article article, onAddInteractor onAddInteractor);
        void uploadImage(Uri image,Context context,OnUploadImageInteractor onUploadImageInteractor);
    }

}
