package com.example.androidminiproject.ui.manipulation;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.androidminiproject.R;
import com.example.androidminiproject.entities.Article;
import com.example.androidminiproject.ui.manipulation.mvp.ManipulateMVP;
import com.example.androidminiproject.ui.manipulation.mvp.ManipulatePresenter;

import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.AppSettingsDialog;
import pub.devrel.easypermissions.EasyPermissions;

public class ManipulateMainActivity extends AppCompatActivity implements ManipulateMVP.View ,EasyPermissions.PermissionCallbacks{

    private Button btnSave;
    private ManipulatePresenter presenter;
    private Uri uri;
    public ImageView image;
    private TextView title;
    private TextView description;
    private ProgressBar progressBar;
    private final static int GALLERY_REQUEST_CODE = 1111;
    private final static int GALLERY_PERMISSION_REQUEST_CODE = 1111;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manipulate_main);

        presenter = new ManipulatePresenter(this);
        initializeView();
        switchLayout(getIntent().getIntExtra("layout",0));

    }

    private void switchLayout(int layoutId){
        switch (layoutId){
            case R.string.add_ui:
                addArticle();
                break;
            case R.string.edit_ui:
                editArticle();
                break;
        }
    }
    //Initialize view
    private void initializeView(){
        btnSave = findViewById(R.id.btn_save);
        title = findViewById(R.id.edit_title);
        description = findViewById(R.id.edit_description);
        image = findViewById(R.id.article_cover_image);
        progressBar = findViewById(R.id.pgb_load_data);


        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                accessToGallery();
            }
        });

    }
    @AfterPermissionGranted(GALLERY_PERMISSION_REQUEST_CODE)
    private void accessToGallery(){
        String[] permission = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA
        };
        if(EasyPermissions.hasPermissions(this,permission)){
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent,GALLERY_REQUEST_CODE);
        }else {
            EasyPermissions.requestPermissions(this,"Please allow us to pick image"
                    ,GALLERY_PERMISSION_REQUEST_CODE,permission);
        }
    }

    //Add new Article
    private void addArticle(){
        btnSave.setText("ADD");
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValid()){
                    add(null);
                    Log.e("TAG","article is added");
                    finish();
                }
                else
                    Toast.makeText(ManipulateMainActivity.this, "Input valid value", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private  void add(String imagePath){
        presenter.addArticle(new Article(
                0,
                title.getText().toString(),
                "11,10,1997",
                description.getText().toString(),
                imagePath
        ));
    }


    //Update article
    private void editArticle(){
        btnSave.setText("Update");
        Log.e("Error", "editView: " + getIntent().getIntExtra("articleId", 0) );
        final int id = getIntent().getIntExtra("articleId",1);
        presenter.findArticleById(id);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(uri != null)
                    presenter.uploadImage(uri,getApplicationContext());
                else{
                    if (isValid()){
                        update(id,"");
                        finish();
                    }
                    else
                        Toast.makeText(ManipulateMainActivity.this, "Please input all fields", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void update(int id, String imagePath){
        presenter.updateArticle(id,new Article(
                0,
                title.getText().toString(),
                "10/11/1975",
                description.getText().toString(),
                imagePath
        ));
    }

    @Override
    public void onShowLoading() {

    }

    @Override
    public void onHideLoading() {

    }

    @Override
    public void onGetSuccess(Article article) {
        if (article != null) {
            title.setText(article.getTitle());
            description.setText(article.getDescription());
        }
    }

    @Override
    public void onFailure(Throwable t) {

    }

    @Override
    public void onUploadImageSuccess(String imagePath) {
        //save
        if(btnSave.getText().equals("ADD")){
            add(imagePath);
        }else {
            int id = getIntent().getIntExtra("articleId",1);
            update(id,imagePath);
        }
    }

    @Override
    public void onFinishView() {

    }

    private boolean isValid(){
        return title.getText().toString().length() > 1 && description.getText().toString().length()>1;
    }


    //Pick image
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode,permissions,grantResults,this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        EasyPermissions.requestPermissions(this,"We need to pick image",GALLERY_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        if(EasyPermissions.somePermissionPermanentlyDenied(this,perms)){
            new AppSettingsDialog.Builder(this).build().show();
        }

    }
}
