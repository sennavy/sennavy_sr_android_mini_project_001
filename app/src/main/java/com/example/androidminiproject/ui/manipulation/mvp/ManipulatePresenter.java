package com.example.androidminiproject.ui.manipulation.mvp;

import android.content.Context;
import android.net.Uri;

import com.example.androidminiproject.entities.Article;
import com.example.androidminiproject.entities.response.ResponseArticle;

public class ManipulatePresenter implements ManipulateMVP.Presenter {
    private ManipulateMVP.Interactor interactor;
    private ManipulateMVP.View view;

    public ManipulatePresenter(ManipulateMVP.View view) {
        this.interactor = new ManipulateInteractor();
        this.view = view;
    }

    @Override
    public void findArticleById(int id) {
        if(view != null){
            view.onShowLoading();
            interactor.getArticleById(id, new ManipulateMVP.Interactor.OnGetArticleInteractor() {
                @Override
                public void onGetSuccess(ResponseArticle responseArticle) {
                    view.onGetSuccess(responseArticle.getArticles());
                    view.onHideLoading();
                }
                @Override
                public void onGetFail(Throwable t) {
                    view.onFailure(t);
                }
            });
        }
    }

    @Override
    public void addArticle(Article article) {
        view.onShowLoading();
        interactor.addArticle(article, new ManipulateMVP.Interactor.onAddInteractor() {
            @Override
            public void onAddSuccess() {
                view.onFinishView();
            }

            @Override
            public void onAddFail(Throwable t) {
                view.onFailure(t);
            }
        });
    }

    @Override
    public void updateArticle(int id, Article article) {
        view.onShowLoading();
        interactor.updateArticle(id, article, new ManipulateMVP.Interactor.OnUpdateInteractor() {
            @Override
            public void onUpdateSuccess() {
                view.onFinishView();
            }

            @Override
            public void onUpdateFail(Throwable t) {
                view.onShowLoading();
                view.onFailure(t);
            }
        });
    }

    @Override
    public void uploadImage(Uri image, Context context) {
        interactor.uploadImage(image, context, new ManipulateMVP.Interactor.OnUploadImageInteractor() {
            @Override
            public void onUploadImageSuccess(String imagePath) {
                view.onUploadImageSuccess(imagePath);
                view.onFinishView();
            }

            @Override
            public void onUploadImageFail(Throwable t) {
                view.onFailure(t);
            }
        });

    }
}
