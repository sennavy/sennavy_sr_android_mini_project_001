package com.example.androidminiproject.ui.main.mvp;

import com.example.androidminiproject.entities.Article;

import java.util.List;

public interface LoadDataMVP {
    interface ArticleView{
        void onShowLoading();
        void onHideLoading();
        void onSetDataToRecyclerView(List<Article> articleResponse);
        void onFail(Throwable t);
    }

    interface Presenter{
        void requestDataFromAPI();
        void onDeleteArticle(int id);
    }

    interface Interactor{
        interface OnGetArticleInteractor{
            void onGetSuccess(List<Article> articles);
            void onGetFail(Throwable t);
        }

        void findAllArticles(int page ,OnGetArticleInteractor onGetArticleInteractor);

        interface OnDeleteInteractor{
            void onDeleteSuccess();
            void onDeleteFail(Throwable t);
        }

        void deleteArticle(int id, OnDeleteInteractor onDeleteInteractor );

    }

}
