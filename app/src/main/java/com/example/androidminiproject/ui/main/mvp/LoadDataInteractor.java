package com.example.androidminiproject.ui.main.mvp;

import android.util.Log;

import com.example.androidminiproject.api.ArticleService;
import com.example.androidminiproject.api.ServiceConverter;
import com.example.androidminiproject.entities.Article;
import com.example.androidminiproject.entities.response.ArticleListResponse;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoadDataInteractor implements LoadDataMVP.Interactor {

    ArticleService articleService = ServiceConverter.retrofit().create(ArticleService.class);
    List<Article> articles;

    @Override
    public void findAllArticles(int page, final OnGetArticleInteractor onGetArticleInteractor) {
        Call<ArticleListResponse> call = articleService.findAllArticles(page);
        call.enqueue(new Callback<ArticleListResponse>() {
            @Override
            public void onResponse(Call<ArticleListResponse> call, Response<ArticleListResponse> response) {
                if(response.body() != null){
                    articles = response.body().getArticles();
                    onGetArticleInteractor.onGetSuccess(articles);
                }
            }
            @Override
            public void onFailure(Call<ArticleListResponse> call, Throwable t) {
                onGetArticleInteractor.onGetFail(t);
            }
        });
    }

    @Override
    public void deleteArticle(int id, final OnDeleteInteractor onDeleteInteractor) {
        Call<ResponseBody> call = articleService.deleteArticle(id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                onDeleteInteractor.onDeleteSuccess();
                Log.e("TAG", "Your lovely article has been deleted");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                onDeleteInteractor.onDeleteFail(t);
                Log.e("TAG", "Your lovely article has been deleted");
            }
        });
    }
}
